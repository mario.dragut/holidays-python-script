from pymongo import MongoClient
import requests

URL = ""
YEAR = ""
HEADERS = {
    "X-RapidAPI-Host": "PUT HERE YOUR HOST", 
    "X-RapidAPI-Key": "PUT HERE YOUR KEY"
    }

def database_connection():

    # URI
    uri = "PUT HERE YOUR URI"
    client = MongoClient(uri)
    
    # DATABASE NAME
    return client["PUT HERE YOUR DB NAME"]

database = database_connection()
collection_name = database["holidays"]

for i in range(22, 101):
    if i < 10:
        YEAR = f"200{i}"
    elif i > 10 and i < 100:
        YEAR = f"20{i}"
    elif i > 99:
        YEAR = f"2{i}"

    URL = f"https://public-holiday.p.rapidapi.com/{YEAR}/RO"
    data = requests.get(url = URL, headers = HEADERS)
    holiday = data.json()

    collection_name.insert_one({ 
        "year": YEAR,
        "holidays": holiday
    })

    print(f"The {YEAR} holidays have been inserted.")
